import {defineField, defineType} from 'sanity'
import {MdStore as icon} from 'react-icons/md'

export default defineType({
  name: 'category',
  title: 'Category',
  type: 'document',
  icon,
  fields: [
    defineField({
      name: 'title',
      title: 'Title',
      type: 'string',
    }),
    defineField({
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'title',
        maxLength: 100,
      },
    }),
  ],
})

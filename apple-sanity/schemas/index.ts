import blockContent from './blockContent'
import crewMember from './crewMember'
import castMember from './castMember'
import product from './product'
import category from './category'
import plotSummary from './plotSummary'
import plotSummaries from './plotSummaries'
import {user, account} from 'next-auth-sanity/schemas'

export const schemaTypes = [
  // Document types
  product,
  category,
  // Other types
  blockContent,
  user,
  account,
]
